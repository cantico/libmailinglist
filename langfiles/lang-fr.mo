��    (      \  5   �      p     q  %        �  
   �     �     �     �     �                    7     =     C     P     ^     r     {     �     �     �     �     �     �     �     �  	   �          &     @  O   Z  )   �     �     �     �     �  (     )   7      a  t  �     �  @   	     G	     f	     }	     �	  #   �	     �	  	   �	     �	  7   
     <
     B
     I
     \
     p
     �
     �
     �
     �
     �
  "   �
     �
  *   �
  $   !     F     b  3   j     �     �  6   �  .        3     8     D  )   [  8   �  1   �  ,   �                 	             #          &   $                           (                
                             %          '                        !                "           Access denied Add "%s", "%s" and "%s" menu entries. Add a mailing list Add a user Add an external recipient Add recipients Add users by group Administration menu Delete Edit Edit your subscriptions Email Group Mailing list Mailing lists Manage mailing list Managing Name Not subsribed Recipient list Rights Rights update. Save Show sitemap entries Subscribe to mailing lists Subscription update. Subsriber The list name can not be empty The name can not be empty The type can not be empty This will remove this mailing list and all linked user will be unlink, proceed? This will remove this recipient, proceed? Type User View subscribers Who can manage mailing lists ? Who can manage this mailing list member? Who can subscribe to this mailing lists ? Who can use this mailing lists ? Project-Id-Version: LibMailingList
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-03-12 18:27+0100
PO-Revision-Date: 2018-03-12 18:27+0100
Last-Translator: Laurent Choulette <laurent.choulette@cantico.fr>
Language-Team: Cantico <support@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ../programs
X-Poedit-KeywordsList: LibMailingList_translate;LibMailingList_translate:1,2
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: api
 Accès refusé Ajoute les menus "%s", "%s" et "%s" dans la section utilisateur. Ajouter une liste de diffusion Ajouter un utilisateur Ajouter un destinataire externe Ajouter des destinataires Ajouter des utilisateurs par groupe Menu d'administration Supprimer Modifier Mettez à jour vos inscriptions aux listes de diffusion Email Groupe Liste de diffusion Listes de diffusion Gestion des listes de diffusion Gestion Nom Non inscrit Liste des destinataires Droits Les droits ont été  mis à jour. Enregistrer Afficher les entrées dans le plan de site Inscriptions aux listes de diffusion Inscriptions misent à jour Inscrit Le nom de la liste de diffusion ne peux être vide. Le nom est obligatoire Le type est obligatoire Cela va supprimer cette liste de diffusion, procéder? Cela va supprimer ce destinataire, procéder ? Type Utilisateur Voir les destinataires Qui peux gérer les listes de diffusion ? Qui peux gérer les membre de cette liste de diffusion ? Qui peux s'inscrire à cette liste de diffusion ? Qui peux utiliser cette liste de diffusion ? 