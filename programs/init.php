<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';






function LibMailingList_upgrade($sVersionBase, $sVersionIni)
{
    require_once dirname(__FILE__).'/functions.php';
    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
    require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';

    $synchronize = new bab_synchronizeSql();
    $synchronize->addOrmSet(LibMailingList_ListSet());
    $synchronize->addOrmSet(LibMailingList_RecipientSet());
    $synchronize->addOrmSet(LibMailingList_MailjetidSet());
    $synchronize->updateDatabase();

    $set = LibMailingList_ListSet();
    $lists = $set->select($set->type->is(''));
    foreach ($lists as $list) {
        $list->type = 'Func_MailingList_Ovidentia';
        $list->save();
    }

    $functionalities = new bab_functionalities();

    $addon = bab_getAddonInfosInstance('LibMailingList');
    $addon->addEventListener('bab_eventBeforeSiteMapCreated', 'LibMailingList_onSiteMapItems', 'init.php');

    $functionalities->registerClass('Func_MailingList'			, $addon->getPhpPath() . '/mailinglist.class.php');
    $functionalities->registerClass('Func_MailingList_Ovidentia', $addon->getPhpPath() . '/api/ovidentia/ovidentia.class.php');
    $functionalities->registerClass('Func_MailingList_Mailjet'	, $addon->getPhpPath() . '/api/mailjet/mailjet.class.php');

    aclCreateTable('LibMailingList_manager_groups');
    aclCreateTable('LibMailingList_membermanager_groups');
    aclCreateTable('LibMailingList_listuser_groups');
    aclCreateTable('LibMailingList_subscriber_groups');

    return true;
}


function LibMailingList_onDeleteAddon()
{
    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
    $functionalities = new bab_functionalities();
    $functionalities->unregister('MailingList');
    return true;
}





/**
 * Sitemap creation
 * @param bab_eventBeforeSiteMapCreated $event
 * @return mixed
 */
function LibMailingList_onSiteMapItems(bab_eventBeforeSiteMapCreated $event) {
    require_once dirname(__FILE__).'/functions.php';

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/LibMailingList/');
    $showSitemapEntries = $registry->getValue('showSitemapEntries', true);

    bab_functionality::includefile('Icons');

    $position = array('root', 'DGAll', 'babUser', 'babUserSectionAddons');

    if ($showSitemapEntries) {
        if (LibMailingList_isManager()) {
            $link = $event->createItem('lib_mailinglist');
            $link->setLabel(LibMailingList_translate('Manage mailing list'));
            $link->setLink('?tg=addon/LibMailingList/main&idx=admin.home');
            $link->setPosition($position);
            $link->addIconClassname(Func_Icons::ACTIONS_GO_HOME);

            $event->addFunction($link);

            $position = array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'lib_mailinglist');

            $link = $event->createItem('lib_mailinglist_rights');
            $link->setLabel(LibMailingList_translate('Rights'));
            $link->setLink('?tg=addon/LibMailingList/main&idx=admin.rights');
            $link->setPosition($position);
            $link->addIconClassname(Func_Icons::ACTIONS_SET_ACCESS_RIGHTS);

            $event->addFunction($link);
        }

        if (LibMailingList_isMemberManager()) {
            $link = $event->createItem('lib_mailinglist_list');
            $link->setLabel(LibMailingList_translate('Manage mailing list'));
            $link->setLink('?tg=addon/LibMailingList/main&idx=lists.displayList');
            $link->setPosition($position);
            $link->addIconClassname(Func_Icons::APPS_MAIL);

            $event->addFunction($link);
        }

        if (LibMailingList_isSubscriber()) {
            $position = array('root', 'DGAll', 'babUser', 'babUserSectionAddons');
            $link = $event->createItem('lib_mailinglist_user');
            $link->setLabel(LibMailingList_translate('Subscribe to mailing lists'));
            $link->setLink('?tg=addon/LibMailingList/main&idx=user.lists');
            $link->setPosition($position);
            $link->addIconClassname(Func_Icons::APPS_MAIL);

            $event->addFunction($link);
        }
    }
}