<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/../../functions.php';
require_once dirname(__FILE__).'/../../mailinglist.class.php';

class Func_MailingList_Ovidentia extends Func_MailingList
{
    public function getDescription()
    {
        return 'Ovidentia Mailing List';
    }

    public function isConfigured()
    {
        return true;
    }

    public function addList($name, $id)
    {
        return true;
    }

    public function updateList($id, $name)
    {
        return true;
    }

    /**
     * Delete an existing list name
     *
     * @param string	$id
     * @param string	$name
     */
    public function deleteList($id)
    {
        $set = LibMailingList_RecipientSet();
        $set->delete($set->list->is($id));

        return true;
    }


    /**
     * Remove
     *
     * @param	LibMailingList_Contact			$contact
     *
     * @return bool
     */
    public function removeContact(LibMailingList_Contact $contact)
    {
        $set = LibMailingList_RecipientSet();

        $set->delete(
            $set->email->is($contact->getEmail())
        );

        return true;
    }


    /**
     * Update contact infos (email, name)
     *
     * @param	LibMailingList_Contact			$old
     * @param	LibMailingList_Contact			$new
     *
     * @return bool
     */
    public function updateContact($old, $new)
    {
        $set = LibMailingList_RecipientSet();

        $recipients = $set->select(
            $set->email->is($old->getEmail())
        );

        foreach ($recipients as $recipient) {
            $recipient->email = $new->getEmail();
            $recipient->save();
        }

        return true;
    }


    /**
     * Get all subscribed mailinglist of a contact
     *
     * @param	LibMailingList_Contact			$contact
     *
     * @return bool
     */
    public function getContactSubscriptions(LibMailingList_Contact $contact)
    {
        $set = LibMailingList_RecipientSet();
        $set->list();
        $mailjets = $set->select($set->email->is($contact->getEmail()));

        $lists = array();
        foreach ($mailjets as $mailjet) {
            $lists[$mailjet->list->id] = $mailjet->list->name;
        }
        return $lists;
    }


    /**
     * Create a new recupient
     * If the contact allready exists in one of the lists, the action is redone
     *
     * @param	LibMailingList_Contact			$contact
     * @param	integer							$list
     *
     * @return bool
     */
    public function subscribeContact(LibMailingList_Contact $contact, $list)
    {
        $set = LibMailingList_RecipientSet();

        $set->delete(
            $set->email->is($contact->getEmail())
            ->_AND_($set->list->is($list))
        );

        $recipient = $set->newRecord();
        $recipient->list = $list;
        $recipient->email = $contact->getEmail();
        $recipient->name = $contact->getName();

        $recipient->save();

        return true;
    }


    /**
     * Create a new recupient
     * If the contact allready exists in one of the lists, the action is redone
     *
     * @param	array(array('email', 'name'))	$contacts
     * @param	integer							$list
     *
     * @return bool
     */
    public function subscribeContacts($contacts, $list)
    {
        $set = LibMailingList_RecipientSet();

        foreach($contacts as $contact) {
            $set->delete(
                $set->email->is($contact['email'])
                ->_AND_($set->list->is($list))
            );

            $recipient = $set->newRecord();
            $recipient->list = $list;
            $recipient->email = $contact['email'];
            $recipient->name = $contact['name'];

            $recipient->save();
        }

        return true;
    }


    /**
     * Create a new recupient
     * If the contact allready exists in one of the lists, the action is redone
     *
     * @param	LibMailingList_Contact			$contact
     * @param	integer							$list
     *
     * @return bool
     */
    public function unsubscribeContact(LibMailingList_Contact $contact, $list)
    {
        $set = LibMailingList_RecipientSet();

        $set->delete(
            $set->email->is($contact->getEmail())
            ->_AND_($set->list->is($list))
        );

        return true;
    }


    /**
     * Create a new recupient
     * If the contact allready exists in one of the lists, the action is redone
     *
     * @param	array(array('email', 'name'))	$contacts
     * @param	integer							$list
     *
     * @return bool
     */
    public function unsubscribeContacts($contacts, $list)
    {
        $set = LibMailingList_RecipientSet();

        foreach($contacts as $contact) {
            $set->delete(
                $set->email->is($contact['email'])
                ->_AND_($set->list->is($list))
            );
        }

        return true;
    }

    /**
     * Get the list recipients
     * return an array where keys are mailling list id, values are malling lists names (label visible by user)
     * return null if the contact does not exists
     * @return array of LibMailingList_contact
     */
    public function getListSubscriptions($list)
    {
        $set = LibMailingList_RecipientSet();
        $lists = $set->select(
            $set->list->is($list)
        );

        $return = array();
        foreach($lists as $list) {
            $return[$list->id] = new LibMailingList_contact();
            $return[$list->id]->setEmail($list->email);
            $return[$list->id]->setName($list->name);
            //$return[$list->id]->setUser($list->user);
        }
        return $return;
    }

    /**
     * Is user sub to the specified list
     *
     * @return bool
     */
    public function isSubscribed(LibMailingList_Contact $contact, $list)
    {
        $set = LibMailingList_RecipientSet();
        $sub = $set->get(
            $set->list->is($list)
            ->_AND_($set->email->is($contact->getEmail()))
        );

        if (!$sub) {
            return false;
        }

        return true;
    }

}

