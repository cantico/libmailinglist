<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/../../functions.php';
require_once dirname(__FILE__).'/../../mailinglist.class.php';

class Func_MailingList_Mailjet extends Func_MailingList
{
    public function getDescription()
    {
        return 'Mailjet Mailing List';
    }

    /**
     * @return Func_Newsletter
     */
    private function getNewsletter()
    {
        return bab_functionality::get('Newsletter/Mailjet');
    }

    public function isConfigured()
    {
        $func = $this->getNewsletter();
        return $func->isConfigured();
    }


    private function getMailjetId($list)
    {
        $set = LibMailingList_MailjetidSet();
        $mailjetid = $set->get($set->list->is($list));
        if(!$mailjetid || $mailjetid->mailjetid == 0) {
            if($mailjetid) {
                $set->delete($set->list->is($list));
            }
            $listSet = LibMailingList_ListSet();
            $currentlist = $listSet->get($listSet->id->is($list));

            $NL = $this->getNewsletter();
            if($idMailjet = $NL->addList($currentlist->name) === false){
                return false;
            }
            $mailjetid = $set->newRecord();
            $mailjetid->list = $list;
            $mailjetid->mailjetid = $idMailjet;
            $mailjetid->save();
        }
        return $mailjetid->mailjetid;
    }

    /**
     * Create a new list
     *
     * @param string	$name
     * @param string	$type
     */
    public function addList($name, $id)
    {
        $NL = $this->getNewsletter();
        if($idMailjet = $NL->addList($name)) {
            $set = LibMailingList_MailjetidSet();
            $mailjetid = $set->newRecord();
            $mailjetid->list = $id;
            $mailjetid->mailjetid = $idMailjet;
            $mailjetid->save();
        }

        return $id;
    }

    /**
     * Update an existing list name
     *
     * @param string	$id
     * @param string	$name
     */
    public function updateList($id, $name)
    {
        $mailjetId = $this->getMailjetId($id);
        if(!$mailjetId) {
            return false;
        }
        $NL = $this->getNewsletter();
        return $NL->updateList($mailjetId, $name);
    }

    /**
     * Delete an existing list name
     *
     * @param string	$id
     * @param string	$name
     */
    public function deleteList($id)
    {
        $mailjetId = $this->getMailjetId($id);
        if(!$mailjetId) {
            return false;
        }
        $NL = $this->getNewsletter();
        $NL->deleteList($mailjetId);

        $set = LibMailingList_MailjetidSet();
        $set->delete($set->list->is($id));

        return true;
    }


    /**
     * Create a new contact
     *
     * @param	LibMailingList_Contact			$contact
     * @param	integer							$list
     *
     * @return bool
     */
    public function subscribeContact(LibMailingList_Contact $contact, $list)
    {
        $mailjetId = $this->getMailjetId($list);
        if(!$mailjetId) {
            return false;
        }
        $NL = $this->getNewsletter();
        return $NL->subscribeContact($contact->getEmail(), $contact->getName(), $mailjetId, true);
    }


    /**
     * Create new contacts
     *
     * @param	array(LibMailingList_Contact)	$contacts
     * @param	integer							$list
     *
     * @return bool
     */
    public function subscribeContacts($contacts, $list)
    {
        $mailjetId = $this->getMailjetId($list);
        if(!$mailjetId) {
            return false;
        }
        $NL = $this->getNewsletter();
        return $NL->subscribeContacts($contacts, $mailjetId, true);
    }


    /**
     * Remove
     *
     * @param	LibMailingList_Contact			$contact
     *
     * @return bool
     */
    public function removeContact(LibMailingList_Contact $contact)
    {
        $NL = $this->getNewsletter();
        return $NL->removeContact($contact->getEmail());
    }


    /**
     * Update contact infos (email, name)
     *
     * @param	LibMailingList_Contact			$old
     * @param	LibMailingList_Contact			$new
     *
     * @return bool
     */
    public function updateContact($old, $new)
    {
        $NL = $this->getNewsletter();
        return $NL->updateContactEmail($old->getEmail(), $new->getEmail());
    }


    /**
     * Get all subscribed mailinglist of a contact
     *
     * @param	LibMailingList_Contact			$contact
     *
     * @return bool
     */
    public function getContactSubscriptions(LibMailingList_Contact $contact)
    {
        $lists = array();

        $NL = $this->getNewsletter();
        $subs = $NL->getContactSubscriptions($contact->getEmail());

        if ($subs) {
            $set = LibMailingList_MailjetidSet();
            $set->list();
            $mailjets = $set->select($set->mailjetid->in(array_keys($subs)));

            foreach ($mailjets as $mailjet) {
                $lists[$mailjet->list->id] = $mailjet->list->name;
            }
        }
        return $lists;
    }


    /**
     * Create a new recupient
     * If the contact allready exists in one of the lists, the action is redone
     *
     * @param	LibMailingList_Contact			$contact
     * @param	integer							$list
     *
     * @return bool
     */
    public function unsubscribeContact(LibMailingList_Contact $contact, $list)
    {
        $mailjetId = $this->getMailjetId($list);
        if(!$mailjetId) {
            return false;
        }
        $NL = $this->getNewsletter();
        return $NL->unsubscribeContact($contact->getEmail(), $mailjetId);
    }


    /**
     * Create new contacts
     *
     * @param	array(LibMailingList_Contact)	$contacts
     * @param	integer							$list
     *
     * @return bool
     */
    public function unsubscribeContacts($contacts, $list)
    {
        $mailjetId = $this->getMailjetId($list);
        if(!$mailjetId) {
            return false;
        }
        $NL = $this->getNewsletter();
        return $NL->unsubscribeContacts($contacts, $mailjetId, true);
    }

    /**
     * Get the list recipients
     * return an array where keys are mailling list id, values are malling lists names (label visible by user)
     * return null if the contact does not exists
     * @return array of LibMailingList_contact
     */
    public function getListSubscriptions($list)
    {
        $mailjetId = $this->getMailjetId($list);
        if(!$mailjetId) {
            return false;
        }
        $NL = $this->getNewsletter();
        $lists = $NL->getListContact($mailjetId);

        $return = array();
        foreach($lists as $list) {
            $return[$list['id']] = new LibMailingList_contact();
            $return[$list['id']]->setEmail($list['email']);
            $return[$list['id']]->setName($list['name']);
            //$return[$list->id]->setUser($list->user);
        }
        return $return;
    }

    /**
     * Is user sub to the specified list
     *
     * @return bool
     */
    public function isSubscribed(LibMailingList_Contact $contact, $list)
    {
        $mailjetId = $this->getMailjetId($list);
        if(!$mailjetId) {
            return false;
        }
        $NL = $this->getNewsletter();
        return $NL->isSubscribedContact($contact->getEmail(), $mailjetId);
    }

}
