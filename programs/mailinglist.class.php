<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/api/ovidentia/ovidentia.class.php';
require_once dirname(__FILE__).'/api/mailjet/mailjet.class.php';

class Func_MailingList extends bab_functionality
{
    public function getDescription()
    {
        return 'Mailing List';
    }

    /**
     * Check if the current user can use at least a mailing list
     *
     * @return bool
     */
    public function canUseMailingList()
    {
        return LibMailingList_isListUser();
    }

    /**
     * Check if the current user can manage members on a specific mailing list
     *
     * mandatory method
     *
     * @return array()
     */
    public function canManageMemberList($list)
    {
        return LibMailingList_canManageMember($list);
    }

    /**
     * Get all usable mailling lists, sorted by name
     * keys are mailling list id, values are malling lists names (label visible by user)
     *
     * mandatory method
     *
     * @return array()
     */
    public function getUsableLists()
    {
        $set = LibMailingList_ListSet();
        $lists = $set->select($set->id->in(LibMailingList_getUsableLists()));
        $lists->orderAsc($set->name);

        $return = array();
        foreach($lists as $list) {
            $return[$list->id] = $list->name;
        }

        return $return;
    }

    /**
     * Get all mailling lists the current can mange member on, sorted by name
     * keys are mailling list id, values are malling lists names (label visible by user)
     *
     * mandatory method
     *
     * @return array()
     */
    public function getManageMemberLists()
    {
        $set = LibMailingList_ListSet();
        $lists = $set->select($set->id->in(LibMailingList_getManageableMemberLists()));
        $lists->orderAsc($set->name);

        $return = array();
        foreach($lists as $list) {
            $return[$list->id] = $list->name;
        }

        return $return;
    }

    /**
     * Get all mailling lists the current user can manage his subscription
     * keys are mailling list id, values are malling lists names (label visible by user)
     *
     * mandatory method
     *
     * @return array()
     */
    public function getSubscribableLists()
    {
        $set = LibMailingList_ListSet();
        $lists = $set->select($set->id->in(LibMailingList_getSubscriberLists()));
        $lists->orderAsc($set->name);

        $return = array();
        foreach($lists as $list) {
            $return[$list->id] = $list->name;
        }

        return $return;
    }

    /**
     * Get all mailling lists
     * keys are mailling list id, values are malling lists names (label visible by user)
     *
     * mandatory method
     *
     * @return array()
     */
    public function getLists()
    {
        $set = LibMailingList_ListSet();
        $lists = $set->select();
        $lists->orderAsc($set->name);

        $return = array();
        foreach($lists as $list) {
            $return[$list->id] = $list->name;
        }

        return $return;
    }

    /**
     * Create a new list
     *
     * @param string	$name
     * @param string	$type
     */
    public function addList($name, $type = 'Func_MailingList_Ovidentia')
    {
        $set = LibMailingList_ListSet();
        $list = $set->newRecord();
        $list->name = $set->name->input($name);
        $list->type = $set->type->input($type);
        $list->save();
        if($ML = $this->getCurrentMailingListClass($list->id)) {
            $ML->addList($list->name, $list->id);
        }

        return $list->id;
    }

    /**
     * Update an existing list name
     *
     * @param string	$id
     * @param string	$name
     */
    public function updateList($id, $name)
    {
        $set = LibMailingList_ListSet();
        $list = $set->get($set->id->is($id));
        $list->name = $set->name->input($name);
        $list->save();

        if($ML = $this->getCurrentMailingListClass($id)) {
            $ML->updateList($id, $list->name);
        }

        return $list->id;
    }

    /**
     * Delete an existing list
     *
     * @param string	$id
     */
    public function deleteList($id)
    {
        if($ML = $this->getCurrentMailingListClass($id)) {
            $ML->deleteList($id);
        }

        $set = LibMailingList_ListSet();
        $set->delete($set->id->is($id));

        return true;
    }


    public function newContact()
    {
        return new LibMailingList_Contact();
    }


    public function getCurrentMailingListClass($list)
    {
        $set = LibMailingList_ListSet();
        $list = $set->get($set->id->is($list));
        if(!$list) {
            return false;
        }

        $class = $list->type;
        return new $class;
    }


    /**
     * Remove a contact from all mailing lists
     * If the contact allready exists in the list, the action is redone
     *
     * @param	LibMailingList_Contact			$contact
     * @param	integer							$list
     *
     * @return bool
     */
    public function removeContact(LibMailingList_Contact $contact)
    {

        $funcs = @bab_functionality::getFunctionalities('MailingList');
        foreach($funcs as $func) {
            $functionnality = @bab_functionality::get('MailingList/'.$func);
            $functionnality->removeContact($contact);
        }
        return $this;
    }


    /**
     * Update contact infos (email, name)isConfigured
     *
     * @param	LibMailingList_Contact			$old
     * @param	LibMailingList_Contact			$new
     *
     * @return bool
     */
    public function updateContact(LibMailingList_Contact $old, LibMailingList_Contact $new)
    {

        $funcs = @bab_functionality::getFunctionalities('MailingList');
        foreach($funcs as $func) {
            $functionnality = @bab_functionality::get('MailingList/'.$func);
            $functionnality->updateContact($old, $new);
        }
        return $this;
    }


    /**
     * Get all subscribed mailinglist of a contact
     *
     * @param	LibMailingList_Contact			$contact
     *
     * @return bool
     */
    public function getContactSubscriptions(LibMailingList_Contact $contact)
    {
        $lists = array();
        $funcs = @bab_functionality::getFunctionalities('MailingList');
        foreach ($funcs as $func) {
            $functionnality = @bab_functionality::get('MailingList/'.$func);
            if ($functionnality->isConfigured()) {
                $currentLists = $functionnality->getContactSubscriptions($contact);
                foreach ($currentLists as $k => $currentList) {
                    $lists[$k] = $currentList;
                }
            }
        }
        return $lists;
    }




    /**
     * Create a new recipient
     * If the contact allready exists in the list, the action is redone
     *
     * @param	LibMailingList_Contact			$contact
     * @param	integer							$list
     *
     * @return bool
     */
    public function subscribeContact(LibMailingList_Contact $contact, $list)
    {
        $ML = $this->getCurrentMailingListClass($list);
        if($ML === false) {
            return false;
        }
        return $ML->subscribeContact($contact, $list);
    }




    /**
     * Create a new recipients
     * If one contact allready exists in the list, the action is redone
     *
     * @param	array(array('email', 'name'))	$contacts
     * @param	integer							$list
     *
     * @return bool
     */
    public function subscribeContacts($contacts, $list)
    {
        $ML = $this->getCurrentMailingListClass($list);
        if($ML === false) {
            return false;
        }
        return $ML->subscribeContacts($contacts, $list);
    }


    /**
     * Create a new recupient
     * If the contact allready exists in one of the lists, the action is redone
     *
     * @param	LibMailingList_Contact			$contact
     * @param	integer							$list
     *
     * @return bool
     */
    public function unsubscribeContact(LibMailingList_Contact $contact, $list)
    {
        $ML = $this->getCurrentMailingListClass($list);
        if($ML === false) {
            return false;
        }
        return $ML->unsubscribeContact($contact, $list);
    }




    /**
     * Create a new recipients
     * If one contact allready exists in the list, the action is redone
     *
     * @param	array(array('email', 'name'))	$contacts
     * @param	integer							$list
     *
     * @return bool
     */
    public function unsubscribeContacts($contacts, $list)
    {
        $ML = $this->getCurrentMailingListClass($list);
        if($ML === false) {
            return false;
        }
        return $ML->unsubscribeContacts($contacts, $list);
    }

    /**
     * Get the list recipients
     * return an array where keys are mailling list id, values are malling lists names (label visible by user)
     * return null if the contact does not exists
     * @return array of LibMailingList_contact
     */
    public function getListSubscriptions($list)
    {
        $ML = $this->getCurrentMailingListClass($list);
        if($ML === false) {
            return false;
        }
        return $ML->getListSubscriptions($list);
    }

    /**
     * Is user sub to the specified list
     *
     * @return bool
     */
    public function isSubscribed(LibMailingList_Contact $contact, $list)
    {
        $ML = $this->getCurrentMailingListClass($list);
        if($ML === false) {
            return false;
        }
        return $ML->isSubscribed($contact, $list);
    }

}


class LibMailingList_contact {
    public $email = '';
    public $name = '';

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}
