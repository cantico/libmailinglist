<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';
require_once dirname(__FILE__) . '/../ui/recipient.ui.php';

/**
 *
 */
class LibMailingList_CtrlRecipient extends LibMailingList_Controller
{

    public function displayList($id)
    {
        /* @var $ML Func_MailingList */
        $ML = bab_functionality::getOriginal('MailingList');

        if(!$ML->canManageMemberList($id)){
            throw new Exception(LibMailingList_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = $W->BabPage();
        if (LibMailingList_isManager()) {
            $page->addItemMenu('home', LibMailingList_translate('Administration menu'), LibMailingList_Controller()->Admin()->home()->url());
        }
        $page->addItemMenu('displayList', LibMailingList_translate('Mailing list'), LibMailingList_Controller()->Lists()->displayList()->url());
        $page->addItemMenu(__METHOD__, LibMailingList_translate('Recipient list'), LibMailingList_Controller()->Recipient()->displayList($id)->url());
        $page->setCurrentItemMenu(__METHOD__);

        $Layout = $this->recipients($id);

        $page->addItem($Layout);

        return $page;
    }



    public function getFilterSet($filter, $set)
    {
        if (isset($filter['name']) && $filter['name'] !== '')
        {
            $criteria = $criteria->_AND_($set->name->contains($filter['name']));
        }

        if (isset($filter['email']) && $filter['email'] !== '')
        {
            $criteria = $criteria->_AND_($set->email->contains($filter['email']));
        }

        return $criteria;
    }

    public function recipients($id/*, $search = null*/)
    {
        /* @var $ML Func_MailingList */
        $ML = bab_functionality::getOriginal('MailingList');

        if(!$ML->canManageMemberList($id)){
            throw new Exception(LibMailingList_translate('Access denied'));
        }
        $W = bab_Widgets();

        $VboxLayout = $W->VBoxLayout();

        $VboxLayout->addItem(
            $W->FlowItems(
                $W->Link(
                    LibMailingList_translate('Add recipients'),
                    LibMailingList_Controller()->Recipient()->edit($id)
                )->addClass('widget-action-button', 'icon', Func_Icons::ACTIONS_LIST_ADD)
                ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            )->addClass(Func_Icons::ICON_LEFT_24)->addClass('widget-100pc')->addClass('widget-toolbar')
        );

        $tableView = new LibMailingList_RecipientList();
        $tableView->setColumnClasses(array(0 => array('widget-column-thin', 'widget-column-center')));
        $tableView->setId('LibMailingList-list-recipients-'.$id);
        $header = array('_action_' => '', 'name' => LibMailingList_translate('Name'), 'email' => LibMailingList_translate('Email'));
        $tableView->setHeader($header);

        $controller = LibMailingList_Controller()->Recipient();
        /* @var $ML Func_MailingList */
        $ML = bab_functionality::getOriginal('MailingList');
        $lists = $ML->getListSubscriptions($id);
        $content = array();
        foreach ($lists as $recipient) {
            $link = $W->Link(
                $temp = $W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE),
                $controller->delete($id, $recipient->getEmail())
            )->setTitle(LibMailingList_translate('Delete'))
            ->setAjaxAction($controller->delete($id, $recipient->getEmail()), $temp)
            ->setConfirmationMessage(LibMailingList_translate('This will remove this recipient, proceed?'));
            $content[] = array('_action_' => $link, 'name' => $recipient->getName(), 'email' => $recipient->getEmail());
        }

        $tableView->setContent($content);

        $tableView->addClass(Func_Icons::ICON_LEFT_16);

        $tableView->setAjaxAction();

        $VboxLayout->addItem($tableView);

        $VboxLayout->setReloadAction(LibMailingList_Controller()->Recipient()->recipients($id/*, $search*/));

        return $VboxLayout;
    }


    public function edit($id)
    {
        /* @var $ML Func_MailingList */
        $ML = bab_functionality::getOriginal('MailingList');

        if(!$ML->canManageMemberList($id)){
            throw new Exception(LibMailingList_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addItemMenu('displayList', LibMailingList_translate('Recipient list'), LibMailingList_Controller()->Recipient()->displayList($id)->url());
        $page->addItemMenu(__METHOD__, LibMailingList_translate('Add a mailing list'), LibMailingList_Controller()->Recipient()->edit($id)->url());
        $page->setCurrentItemMenu(__METHOD__);

        $editor = new LibMailingList_RecipientEditor($id);
        if(bab_isAjaxRequest()) {
            return $editor;
        }

        $page->addItem($editor);

        return $page;
    }

    public function delete($id = null, $email = null)
    {
        /* @var $ML Func_MailingList */
        $ML = bab_functionality::getOriginal('MailingList');

        if(!$ML->canManageMemberList($id)){
            throw new Exception(LibMailingList_translate('Access denied'));
        }

        $contact  = $ML->newContact();
        $contact->setEmail($email);
        $ML->unsubscribeContact($contact, $id);

        if(bab_isAjaxRequest()) {
            return true;
        }
        LibMailingList_Controller()->Recipient()->displayList()->location();
    }

    public function save($recipient = null)
    {
        /* @var $ML Func_MailingList */
        $ML = bab_functionality::getOriginal('MailingList');

        if(!isset($recipient['list']) || !$ML->canManageMemberList($recipient['list'])){
            throw new Exception(LibMailingList_translate('Access denied'));
        }

        $set = LibMailingList_RecipientSet();

        if(isset($recipient['group']) && $recipient['group']) {
            $groups = bab_getGroups($recipient['group']);
            if(isset($groups['id'])) {
                $groups = $groups['id'];
            }
            $groups[] = $recipient['group'];
            $users = bab_getGroupsMembers($groups);
            foreach ($users as $user) {
                $contact  = $ML->newContact();
                $contact->setEmail($user['email']);
                $contact->setName($user['name']);
                if ($ML->isSubscribed($contact, $recipient['list'])) {
                    $ML->unsubscribeContact($contact, $recipient['list']);
                }
                $ML->subscribeContact($contact, $recipient['list']);
            }
        }
        if(isset($recipient['user']) && $recipient['user']) {
            ;
            $userInfo = bab_getUserInfos($recipient['user']);

            $contact  = $ML->newContact();
            $contact->setEmail(bab_getUserEmail($recipient['user']));
            $contact->setName(bab_getUserName($recipient['user']));
            if ($ML->isSubscribed($contact, $recipient['list'])) {
                $ML->unsubscribeContact($contact, $recipient['list']);
            }
            $ML->subscribeContact($contact, $recipient['list']);
        }
        if(isset($recipient['email']) && $recipient['email']) {
            $contact  = $ML->newContact();
            $contact->setEmail($recipient['email']);
            $contact->setName($recipient['name']);
            if ($ML->isSubscribed($contact, $recipient['list'])) {
                $ML->unsubscribeContact($contact, $recipient['list']);
            }
            $ML->subscribeContact($contact, $recipient['list']);
        }

        return true;
    }
}
