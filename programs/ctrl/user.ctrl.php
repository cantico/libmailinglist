<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';
require_once dirname(__FILE__) . '/../ui/user.ui.php';

/**
 *
 */
class LibMailingList_CtrlUser extends LibMailingList_Controller
{



    public function lists()
    {
        if (!bab_isUserLogged()) {
            bab_requireCredential();
        }
        $W = bab_Widgets();
        $page = $W->BabPage();
        $AI = bab_getAddonInfosInstance('LibMailingList');
        $page->addStyleSheet($AI->getStylePath().'main.css');
        $page->addClass("LibMailingList-editor");

        $page->addItem(
            $W->Title(LibMailingList_translate('Edit your subscriptions'))
        );

        $editor = new LibMailingList_SubscriptionEditor();
        $page->addItem($editor);

        return $page;
    }

    public function saveLists($lists = null)
    {
        global $babBody;

        /* @var $ML Func_MailingList */
        $ML = bab_functionality::getOriginal('MailingList');

        $contact = $ML->newContact();
        $contact->setEmail(bab_getUserEmail(bab_getUserId()));
        $contact->setName(bab_getUserName(bab_getUserId()));

        foreach ($lists['list'] as $id => $status) {
            if($status == 0) {
                if ($ML->isSubscribed($contact, $id)) {
                    $ML->unsubscribeContact($contact, $id);
                }
            } else {
                if ($ML->isSubscribed($contact, $id)) {
                    $ML->unsubscribeContact($contact, $id);
                }
                $ML->subscribeContact($contact, $id);
            }
        }

        $babBody->addNextPageMessage(LibMailingList_translate('Subscription update.'));
        return true;
    }
}
