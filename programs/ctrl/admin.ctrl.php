<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';
require_once dirname(__FILE__) . '/../ui/admin.ui.php';

/**
 *
 */
class LibMailingList_CtrlAdmin extends LibMailingList_Controller
{

	
    public function home()
    {
        if(!LibMailingList_isManager()){
            throw new Exception(LibMailingList_translate('Access denied'));
        }
    	$W = bab_Widgets();
        $page = $W->BabPage();
		$AI = bab_getAddonInfosInstance('LibMailingList');
		$page->addStyleSheet($AI->getStylePath().'main.css');
        $page->addItemMenu('home', LibMailingList_translate('Administration menu'), LibMailingList_Controller()->Admin()->home()->url());
        $page->setCurrentItemMenu('home');

        $HomeFrame = LibMailingList_AdminHome();
        $page->addItem($HomeFrame);

        return $page;
	}

    public function rights()
    {
        if(!LibMailingList_isManager()){
            throw new Exception(LibMailingList_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addItemMenu('home', LibMailingList_translate('Administration menu'), LibMailingList_Controller()->Admin()->home()->url());
        $page->addItemMenu(__METHOD__, LibMailingList_translate('Rights'), LibMailingList_Controller()->Admin()->rights()->url());
        $page->setCurrentItemMenu(__METHOD__);

        $editor = new LibMailingList_AdminEditor();
        $page->addItem($editor);

        return $page;
    }

    public function saveRights($options = array())
    {
        if(!LibMailingList_isManager()){
            throw new Exception(LibMailingList_translate('Access denied'));
        }
        require_once $GLOBALS['babInstallPath'].'admin/acl.php';

        aclSetRightsString('LibMailingList_manager_groups', 1, $options['manager']);
		
		$GLOBALS['babBody']->addNextPageMessage(LibMailingList_translate('Rights update.'));

        return true;
    }

}
