<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../controller.class.php';
require_once dirname(__FILE__) . '/../ui/lists.ui.php';

/**
 *
 */
class LibMailingList_CtrlLists extends LibMailingList_Controller
{


    public function displayList()
    {
        if(!LibMailingList_isMemberManager()){
            throw new Exception(LibMailingList_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = $W->BabPage();
        if (LibMailingList_isManager()) {
            $page->addItemMenu('home', LibMailingList_translate('Administration menu'), LibMailingList_Controller()->Admin()->home()->url());
        }
        $page->addItemMenu(__METHOD__, LibMailingList_translate('Mailing list'), LibMailingList_Controller()->Lists()->displayList()->url());
        $page->setCurrentItemMenu(__METHOD__);

        $Layout = $this->lists();

        $page->addItem($Layout);

        return $page;
    }

    public function lists()
    {
        if(!LibMailingList_isMemberManager()){
            throw new Exception(LibMailingList_translate('Access denied'));
        }
        $W = bab_Widgets();

        $VboxLayout = $W->VBoxLayout();

        if (LibMailingList_isManager()) {
            $VboxLayout->addItem(
                $W->FlowItems(
                    $W->Link(
                        LibMailingList_translate('Add a mailing list'),
                        LibMailingList_Controller()->Lists()->edit()
                    )->addClass('widget-action-button', 'icon', Func_Icons::ACTIONS_LIST_ADD)
                    ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                )->addClass(Func_Icons::ICON_LEFT_24)->addClass('widget-100pc')->addClass('widget-toolbar')
            );
        }

        $tableView = new LibMailingList_MailingList();
        $set = LibMailingList_ListSet();
        $lists = $set->select($set->id->in(LibMailingList_getManageableMemberLists()));
        $lists->orderAsc($set->name);
        $tableView->addClass(Func_Icons::ICON_LEFT_16);
        $tableView->setDataSource($lists);
        $tableView->addDefaultColumns($set);

        $VboxLayout->addItem($tableView);

        $VboxLayout->setReloadAction(LibMailingList_Controller()->Lists()->lists());

        return $VboxLayout;
    }


    public function edit($id = null)
    {
        if(!LibMailingList_isManager()){
            throw new Exception(LibMailingList_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addItemMenu('home', LibMailingList_translate('Administration menu'), LibMailingList_Controller()->Admin()->home()->url());
        $page->addItemMenu('displayList', LibMailingList_translate('Mailing list'), LibMailingList_Controller()->Lists()->displayList()->url());
        $page->addItemMenu(__METHOD__, LibMailingList_translate('Add a mailing list'), LibMailingList_Controller()->Lists()->edit($id)->url());
        $page->setCurrentItemMenu(__METHOD__);

        $editor = new LibMailingList_MailingListEditor($id);
        if(bab_isAjaxRequest()) {
            return $editor;
        }

        $page->addItem($editor);

        return $page;
    }

    public function delete($id = null)
    {
        if(!LibMailingList_isManager()){
            throw new Exception(LibMailingList_translate('Access denied'));
        }

        /* @var $ML Func_MailingList */
        $ML = bab_functionality::getOriginal('MailingList');
        $ML->deleteList($id);

        if(bab_isAjaxRequest()) {
            return true;
        }
        LibMailingList_Controller()->Lists()->displayList()->location();
    }

    public function save($list = null)
    {
        if(!LibMailingList_isManager()){
            throw new Exception(LibMailingList_translate('Access denied'));
        }

        if($list['name'] == ''){
            throw new bab_SaveException(LibMailingList_translate('The list name can not be empty'));
        }

        /* @var $ML Func_MailingList */
        $ML = bab_functionality::getOriginal('MailingList');

        if(isset($list['id']) && $list['id']){
            $ML->updateList($list['id'], $list['name']);
        } else {
            $ML->addList($list['name'], $list['type']);
        }

        return true;
    }

    public function rights($id)
    {
        if(!LibMailingList_isManager()){
            throw new Exception(LibMailingList_translate('Access denied'));
        }
        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addItemMenu('home', LibMailingList_translate('Administration menu'), LibMailingList_Controller()->Admin()->home()->url());
        $page->addItemMenu('displayList', LibMailingList_translate('Mailing list'), LibMailingList_Controller()->Lists()->displayList()->url());
        $page->addItemMenu(__METHOD__, LibMailingList_translate('Rights'), LibMailingList_Controller()->Lists()->rights($id)->url());
        $page->setCurrentItemMenu(__METHOD__);

        $editor = new LibMailingList_ListRightEditor(null, null, $id);
        $page->addItem($editor);

        return $page;
    }

    public function saveRights($id = null, $options = array())
    {
        if(!LibMailingList_isManager() || !$id){
            throw new Exception(LibMailingList_translate('Access denied'));
        }
        require_once $GLOBALS['babInstallPath'].'admin/acl.php';

        aclSetRightsString('LibMailingList_membermanager_groups', $id, $options['member']);
        aclSetRightsString('LibMailingList_listuser_groups', $id, $options['user']);
        aclSetRightsString('LibMailingList_subscriber_groups', $id, $options['subscriber']);

        $GLOBALS['babBody']->addNextPageMessage(LibMailingList_translate('Rights update.'));

        return true;
    }

}
