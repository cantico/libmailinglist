<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';



class LibMailingList_ConfigurationPage
{


    private function getForm()
    {
        $W = bab_Widgets();
        $form = $W->Form();
        $form->setName('configuration')->addClass('BabLoginMenuBackground')->addClass('libmailinglist-form');
        $form->setHiddenValue('tg', bab_rp('tg'));
        $form->colon();

        $form->getLayout()->setVerticalSpacing(1,'em');


        $description = sprintf(
            LibMailingList_translate('Add "%s", "%s" and "%s" menu entries.'),
            LibMailingList_translate('Manage mailing list'),
            LibMailingList_translate('Rights'),
            LibMailingList_translate('Subscribe to mailing lists')
        );


        $form->addItem(
            $W->LabelledWidget(
                LibMailingList_translate('Show sitemap entries'),
                $W->CheckBox(),
                'showSitemapEntries',
                $description
            )
        );

        $form->addItem(
            $W->SubmitButton()
                ->setLabel(LibMailingList_translate('Save'))
        );

        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/LibMailingList/');


        $form->setValues(
            array(
                'configuration' => array(
                    'showSitemapEntries' => $registry->getValue('showSitemapEntries', true),
                )
            )
        );

        return $form;
    }




    public function display()
    {
        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addStyleSheet($GLOBALS['babInstallPath'].'styles/addons/LibMailingList/main.css');

        $page->addItem($this->getForm());
        $page->displayHtml();
    }


    public function save($configuration)
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/LibMailingList/');

        $registry->setKeyValue('showSitemapEntries', $configuration['showSitemapEntries']);
    }
}


if (!bab_isUserAdministrator())
{
    return;
}


$page = new LibMailingList_ConfigurationPage;

if (!empty($_POST))
{
    $page->save(bab_pp('configuration'));
    bab_Sitemap::clearAll();
}

$page->display();
