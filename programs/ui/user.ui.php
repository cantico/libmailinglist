<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once $GLOBALS['babInstallPath']."utilit/dateTime.php";

bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('widget_TableModelView');





class LibMailingList_SubscriptionEditor extends Widget_Form
{
    protected $radio = array();


    public function __construct()
    {
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');

        parent::__construct(null, $layout);

        $this->setName('lists');
        $this->addClass('LibMailingList-subscrib');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->addFields();
        $controller = LibMailingList_Controller()->User();

        $this->addItem(
            $buttonLayout = $W->FlowItems(
                $W->SubmitButton()
                    ->validate()
                    ->setAction($controller->saveLists())
                    ->setSuccessAction($controller->lists())
                    ->setFailedAction($controller->lists())
                    ->setLabel(LibMailingList_translate('Save'))
            )
        );

        $this->loadValues();
    }


    protected function loadValues()
    {

    }


    protected function subscriptions($list)
    {
        $W = bab_Widgets();

        /* @var $ML Func_MailingList */
        $ML = bab_functionality::getOriginal('MailingList');
        $email = bab_getUserEmail(bab_getUserId());
        $contact = $ML->newContact();
        $contact->setEmail($email);

        $value = 0;
        if($ML->isSubscribed($contact, $list)) {
            $value = 1;
        }

        $layout = $W->VBoxItems();

        $radioSet = $W->RadioSet()->setRadioNamePath(
            array('lists', 'list', $list)
        )->setValue($value);

        $this->radio[$list] = $radioSet;

        $layout->addItem(
            $W->LabelledWidget(
                LibMailingList_translate('Not subsribed'),
                $W->Radio(null, $radioSet)->setCheckedValue(0)
            )->addClass('LibMailingList-subscrib-no')
        );
        $layout->addItem(
            $W->LabelledWidget(
                LibMailingList_translate('Subsriber'),
                $W->Radio(null, $radioSet)->setCheckedValue(1)
            )
        );

        return $layout;
    }


    protected function addFields()
    {
        $W = bab_Widgets();

        $this->addItem(
            $layout = $W->FlowLayout()->setVerticalAlign('top')->setVerticalSpacing(1,'em')->setHorizontalSpacing(1,'em')
        );


        /* @var $ML Func_MailingList */
        $ML = bab_functionality::getOriginal('MailingList');
        $lists = $ML->getSubscribableLists();

        foreach($lists as $id => $list) {
            $layout->addItem(
                $W->Section(
                    $list,
                    $this->subscriptions($id)
                )->setSizePolicy('LibMailingList-subscrib-item')
            );
        }
    }
}