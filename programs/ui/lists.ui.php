<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once $GLOBALS['babInstallPath']."utilit/dateTime.php";

bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('widget_TableModelView');




/**
 * Liste des Mailing list
 */
class LibMailingList_MailingList extends widget_TableModelView
{

    public function addDefaultColumns($set)
    {
        $this->addColumn(widget_TableModelViewColumn('_edit_', '')->setSortable(false)->addClass('widget-column-thin')->addClass('widget-column-center'));
        $this->addColumn(widget_TableModelViewColumn($set->name, LibMailingList_translate('Name')));

        return $this;
    }


    /**
     * @param ORM_Record	$record
     * @param string		$fieldPath
     * @return Widget_Item
     */
    protected function computeCellContent(ORM_Record $record, $fieldPath)
    {

        $controller = LibMailingList_Controller();
        $W = bab_Widgets();

        switch ($fieldPath) {
            case '_edit_':
                /* @var $ML Func_MailingList */
                $ML = bab_functionality::getOriginal('MailingList');
                $users = $ML->getListSubscriptions($record->id);

                $layout = $W->HboxItems()->setHorizontalSpacing(.25, 'em')->setVerticalAlign('bottom');
                $layout->addItem(
                    $W->HboxItems(
                        $W->Link(
                            '',
                            $controller->Recipient()->displayList($record->id)
                        )->addClass(Func_Icons::APPS_GROUPS, 'icon')
                        ->setTitle(LibMailingList_translate('View subscribers')),
                        $W->label('('.count($users).')')
                    )->setVerticalAlign('bottom')
                );
                if (LibMailingList_isManager()) {
                    $layout->addItem(
                        $W->Link(
                            '',
                            $controller->Lists()->edit($record->id)
                        )->addClass(Func_Icons::ACTIONS_DOCUMENT_EDIT, 'icon')
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                        ->setTitle(LibMailingList_translate('Edit'))
                    );
                    $layout->addItem(
                        $W->Link(
                            '',
                            $controller->Lists()->rights($record->id)
                        )->addClass(Func_Icons::ACTIONS_SET_ACCESS_RIGHTS, 'icon')
                        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                        ->setTitle(LibMailingList_translate('Rights'))
                    );
                    $layout->addItem(
                        $temp = $W->Link(
                            '',
                            $controller->Lists()->delete($record->id)
                        )->addClass(Func_Icons::ACTIONS_EDIT_DELETE, 'icon')
                        ->setTitle(LibMailingList_translate('Delete'))
                        ->setConfirmationMessage(LibMailingList_translate('This will remove this mailing list and all linked user will be unlink, proceed?'))
                    );
                    $temp->setAjaxAction($controller->Lists()->delete($record->id), $temp);

                }
                return $layout;
                break;
        }

        return parent::computeCellContent($record, $fieldPath);
    }

}


class LibMailingList_MailingListEditor extends Widget_Form
{
    public function __construct($list = null)
    {
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');

        parent::__construct(null, $layout);

        $this->setName('list');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));

        $this->list = $list;
        if($this->list === null) {
            $this->list = '';
        }

        $this->addFields();

        if(bab_isAjaxRequest()){
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAjaxAction(LibMailingList_Controller()->Lists()->save())
                )
            );
        }else{
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAction(LibMailingList_Controller()->Lists()->save())
                        ->setSuccessAction(LibMailingList_Controller()->Lists()->displayList())
                        ->setFailedAction(LibMailingList_Controller()->Lists()->edit())
                        ->setLabel(LibMailingList_translate('Save'))
                )
            );
        }

        if($this->list && !bab_isAjaxRequest()){
            $buttonLayout->addItem(
                $W->Link(
                    $W->Icon(LibMailingList_translate('Delete'), Func_Icons::ACTIONS_EDIT_DELETE),
                    LibMailingList_Controller()->Lists()->delete($this->list)
                )->addClass(Func_Icons::ICON_LEFT_16)
            )->setHorizontalSpacing(1, 'em');
        }

        $this->loadValues();
    }


    protected function loadValues()
    {
        $this->setHiddenValue('list[id]', $this->list);
        if($this->list){
            $set = LibMailingList_ListSet();
            $list = $set->get($this->list);
            if($list){
                $this->setValues($list->getValues(), array('list'));
            }
        }
    }

    protected function name()
    {
        $W = bab_Widgets();
        return $W->LabelledWidget(
            LibMailingList_translate('Name'),
            $W->LineEdit()->setMandatory(true, LibMailingList_translate('The name can not be empty')),
            'name'
        )->colon(true);
    }

    protected function type()
    {
        $W = bab_Widgets();

        $options = array();

        /* @var $func Func_Newsletter */
        $funcs = @bab_functionality::getFunctionalities('MailingList');
        foreach($funcs as $func) {
            $functionnality = @bab_functionality::get('MailingList/'.$func);
            $options[get_class($functionnality)] = $functionnality->getDescription();
        }

        return $W->LabelledWidget(
            LibMailingList_translate('Type'),
            $W->Select()->setOptions($options)->setValue('Func_MailingList_Ovidentia')->setMandatory(true, LibMailingList_translate('The type can not be empty')),
            'type'
        )->colon(true);
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem($this->name());
        if($this->list === '') {
            $this->addItem($this->type());
        }
    }
}





class LibMailingList_ListRightEditor extends Widget_Form
{
    public function __construct($id = null, Widget_Layout $layout = null, $id_list)
    {
        $W = bab_Widgets();

        if (null === $layout)
        {
            $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');
        }

        $this->id_list = $id_list;

        parent::__construct($id, $layout);

        $this->setName('options');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setHiddenValue('id', $this->id_list);

        $this->addFields();

        $this->addItem(
            $W->SubmitButton()
                ->setAction(LibMailingList_Controller()->Lists()->saveRights())
                ->setSuccessAction(LibMailingList_Controller()->Lists()->displayList())
                ->setFailedAction(LibMailingList_Controller()->Lists()->rights($this->id_list))
                ->setLabel(LibMailingList_translate('Save'))
        );

        $this->loadValues();
    }


    protected function loadValues()
    {
        require_once $GLOBALS['babInstallPath'].'admin/acl.php';
        $this->setValue(array('options', 'member'), aclGetRightsString('LibMailingList_membermanager_groups', $this->id_list));
        $this->setValue(array('options', 'user'), aclGetRightsString('LibMailingList_listuser_groups', $this->id_list));
        $this->setValue(array('options', 'subscriber'), aclGetRightsString('LibMailingList_subscriber_groups', $this->id_list));
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem($W->Acl()->setName('member')->setTitle(LibMailingList_translate('Who can manage this mailing list member?')));
        $this->addItem($W->Acl()->setName('user')->setTitle(LibMailingList_translate('Who can use this mailing lists ?')));
        $this->addItem($W->Acl()->setName('subscriber')->setTitle(LibMailingList_translate('Who can subscribe to this mailing lists ?')));
    }



}