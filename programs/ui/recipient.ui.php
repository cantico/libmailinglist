<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once $GLOBALS['babInstallPath']."utilit/dateTime.php";

bab_Widgets()->includePhpClass('Widget_Form');
bab_Widgets()->includePhpClass('widget_TableArrayView');




/**
 * Liste des recipient
 */
class LibMailingList_RecipientList extends widget_TableArrayView
{

}


class LibMailingList_RecipientEditor extends Widget_Form
{
    public function __construct($list)
    {
        $W = bab_Widgets();

        $layout = $W->VBoxLayout()->setVerticalSpacing(1,'em');

        $this->list = $list;

        parent::__construct(null, $layout);

        $this->setName('recipient');
        $this->addClass('BabLoginMenuBackground');
        $this->addClass('widget-bordered');

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setHiddenValue('recipient[list]', $this->list);

        $this->addFields();

        if(bab_isAjaxRequest()){
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAjaxAction(LibMailingList_Controller()->Recipient()->save())
                )
            );
        }else{
            $this->addItem(
                $buttonLayout = $W->FlowItems(
                    $W->SubmitButton()
                        ->validate()
                        ->setAction(LibMailingList_Controller()->Recipient()->save())
                        ->setSuccessAction(LibMailingList_Controller()->Recipient()->displayList($this->list))
                        ->setFailedAction(LibMailingList_Controller()->Recipient()->edit($this->list))
                        ->setLabel(LibMailingList_translate('Save'))
                )
            );
        }

    }


    protected function user()
    {
        $W = bab_Widgets();

        return $W->LabelledWidget(
            LibMailingList_translate('User'),
            $W->UserPicker(),
            'user'
        )->colon(true);
    }


    protected function group()
    {
        $W = bab_Widgets();

        return $W->LabelledWidget(
            LibMailingList_translate('Group'),
            $W->GroupPicker()->setStartGroup(1),
            'group'
        )->colon(true);
    }


    protected function email()
    {
        $W = bab_Widgets();

        return $W->LabelledWidget(
            LibMailingList_translate('Email'),
            $W->EmailLineEdit()->setSize(42),
            'email'
        )->colon(true);
    }


    protected function name()
    {
        $W = bab_Widgets();

        return $W->LabelledWidget(
            LibMailingList_translate('Name'),
            $W->LineEdit(),
            'name'
        )->colon(true);
    }


    protected function addFields()
    {
        $W = bab_Widgets();
        $this->addItem(
            $W->Section(
                LibMailingList_translate('Add a user'),
                $this->user()
            )->setFoldable(true, false)
        );

        $this->addItem(
            $W->Section(
                LibMailingList_translate('Add users by group'),
                $this->group()
            )->setFoldable(true, true)
        );

        $this->addItem(
            $W->Section(
                LibMailingList_translate('Add an external recipient'),
                $W->VBoxItems(
                    $this->email(),
                    $W->FlowItems(
                        $this->name()
                    )->setHorizontalSpacing(1,'em')
                )->setVerticalSpacing(1,'em')
            )->setFoldable(true, true)
        );
    }
}