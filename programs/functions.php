<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


function LibMailingList_translate($str, $str_plurals = null, $number = null)
{
    if ($translate = bab_functionality::get('Translate/Gettext'))
    {
        /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('LibMailingList');

        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}


/**
 * Initialize mysql ORM backend.
 */
function LibMailingList_loadOrm()
{
    static $loadOrmDone = false;

    if (!$loadOrmDone) {

        $Orm = bab_functionality::get('LibOrm');
        /*@var $Orm Func_LibOrm */
        $Orm->initMySql();

        $mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
        ORM_MySqlRecordSet::setBackend($mysqlbackend);

        $loadOrmDone = true;
    }
}


function LibMailingList_isManager()
{
    return bab_isAccessValid('LibMailingList_manager_groups', 1) || bab_isUserAdministrator();
}

//MANAGE MEMBER
function LibMailingList_isMemberManager()
{
    if(LibMailingList_isManager()) {
        return true;
    }

    $set = LibMailingList_ListSet();
    $lists = $set->select();
    foreach ($lists as $list) {
        if(bab_isAccessValid('LibMailingList_membermanager_groups', $list->id)) {
            return true;
        }
    }
    return false;
}

function LibMailingList_canManageMember($id_list)
{
    return bab_isAccessValid('LibMailingList_membermanager_groups', $id_list) || LibMailingList_isManager();
}

function LibMailingList_getManageableMemberLists()
{
    $return = array();
    $set = LibMailingList_ListSet();
    $lists = $set->select();
    foreach ($lists as $list) {
        if(LibMailingList_canManageMember($list->id)) {
            $return[] = $list->id;
        }
    }
    return $return;
}

//USE LIST
function LibMailingList_isListUser()
{
    if(LibMailingList_isManager()) {
        return true;
    }

    $set = LibMailingList_ListSet();
    $lists = $set->select();
    foreach ($lists as $list) {
        if(bab_isAccessValid('LibMailingList_listuser_groups', $list->id)) {
            return true;
        }
    }
    return false;
}

function LibMailingList_canUseList($id_list)
{
    return bab_isAccessValid('LibMailingList_listuser_groups', $id_list) || LibMailingList_isManager();
}

function LibMailingList_getUsableLists()
{
    $return = array();
    $set = LibMailingList_ListSet();
    $lists = $set->select();
    foreach ($lists as $list) {
        if(LibMailingList_canUseList($list->id)) {
            $return[] = $list->id;
        }
    }
    return $return;
}


//SUBSCRIBE

function LibMailingList_isSubscriber()
{
    $set = LibMailingList_ListSet();
    $lists = $set->select();
    foreach ($lists as $list) {
        if(LibMailingList_canSubscribeList($list->id)) {
            return true;
        }
    }
    return false;
}

function LibMailingList_canSubscribeList($id_list)
{
    if (!bab_getUserId()) {
        return false;
    }
    if(bab_isAccessValid('LibMailingList_subscriber_groups', $id_list)) {
        return true;
    }
    $set = LibMailingList_RecipientSet();
    $recipient = $set->get(
        $set->list->is($id_list)
        ->_AND_($set->email->is(bab_getUserEmail(bab_getUserId())))
    );
    if ($recipient) {
        return true;
    }
    return false;
}

function LibMailingList_getSubscriberLists()
{
    $return = array();
    $set = LibMailingList_ListSet();
    $lists = $set->select();
    foreach ($lists as $list) {
        if(LibMailingList_canSubscribeList($list->id)) {
            $return[] = $list->id;
        }
    }
    return $return;
}



/**
 * @return LibMailingList_Controller
 */
function LibMailingList_Controller()
{
    require_once dirname(__FILE__) . '/controller.class.php';
    return bab_getInstance('LibMailingList_Controller');
}


function LibMailingList_IncludeSet()
{
    require_once dirname(__FILE__).'/set/list.class.php';
    require_once dirname(__FILE__).'/api/ovidentia/set/recipient.class.php';
    require_once dirname(__FILE__).'/api/mailjet/set/mailjetid.class.php';
}

/**
 * @return LibMailingList_ListSet
 */
function LibMailingList_ListSet()
{
    LibMailingList_IncludeSet();

    return new LibMailingList_ListSet();
}

/**
 * @return LibMailingList_RecipientSet
 */
function LibMailingList_RecipientSet()
{
    LibMailingList_IncludeSet();

    return new LibMailingList_RecipientSet();
}

/**
 * @return LibMailingList_MailjetidSet
 */
function LibMailingList_MailjetidSet()
{
    LibMailingList_IncludeSet();

    return new LibMailingList_MailjetidSet();
}
