; <?php/*

[general]
name							="LibMailingList"
version							="2.1.5"
addon_type						="EXTENSION"
encoding						="UTF-8"
description						="Mailing list subscription managment"
description.fr					="Gestion des inscriptions à des lettres diffusion"
delete							=1
ov_version						="7.7.0"
php_version						="5.4.0"
mysql_version					="4.1.2"
addon_access_control			=0
author							="Cantico"
icon							="mail_message_new.png"
mysql_character_set_database	="latin1,utf8"
configuration_page				="admin"

[addons]
jquery							="1.7.1.4"
widgets							="1.0.65"
LibOrm							="0.9.4"
LibNewsletter					="1.2.0"

[functionalities]
Newsletter						="Available"
;*/?>